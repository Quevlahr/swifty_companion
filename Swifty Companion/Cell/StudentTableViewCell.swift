//
//  StudentTableViewCell.swift
//  Swifty Companion
//
//  Created by Quentin Roulon on 27/10/2018.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelIndicatorProgressView: UIProgressView!
    
    func displayStudent(skill: Skill) {
        self.nameLabel.text = skill.name
        self.levelLabel.text = String(skill.level)
        self.levelIndicatorProgressView.progress = Float(skill.level.truncatingRemainder(dividingBy: 1))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
