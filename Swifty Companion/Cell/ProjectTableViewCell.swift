//
//  ProjectTableViewCell.swift
//  Swifty Companion
//
//  Created by Quentin Roulon on 28/10/2018.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    
    func displayProject(project: Project) {
        projectNameLabel.text = project.project.name
        percentLabel.text = String(project.final_mark ?? 0) + "%"
//        percentLabel.text = String(project.validated ?? 0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
