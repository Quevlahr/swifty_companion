//
//  StudentViewController.swift
//  Swifty Companion
//
//  Created by Quentin ROULON on 10/15/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

protocol CursusProtocol {
    func cursusIsChoosen(name: String)
}

class StudentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, CursusProtocol {

    /**
     *   VARIABLES
     **/
    
    var student: Student!
    var currentCursusId: Int = 0
    var currentProjects: [Project] = []
    var currentSkills: [Skill] = []
    var listCursus = [String]()
    
    @IBOutlet weak var cursusTextField: UITextField! {
        didSet {
            self.cursusTextField.delegate = self
        }
    }
    @IBOutlet weak var dropDownPickerView: UIPickerView! {
        didSet {
            self.dropDownPickerView.delegate = self
            self.dropDownPickerView.dataSource = self
        }
    }
    
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var studentImageView: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var walletLabel: UILabel!
    @IBOutlet weak var correctionPointLabel: UILabel!
    
    @IBOutlet weak var skillTableView: UITableView! {
        didSet {
            skillTableView.delegate = self
            skillTableView.dataSource = self
        }
    }
    
    @IBOutlet weak var projectTableView: UITableView! {
        didSet {
            projectTableView.delegate = self
            projectTableView.dataSource = self
        }
    }
    
    /**
    *   DROPDOWN
    **/
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listCursus.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.view.endEditing(true)
        return listCursus[row]
    }
    
    // A cursus is choosen
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.cursusIsChoosen(name: self.listCursus[row])
        self.cursusTextField.text = self.listCursus[row]
        self.dropDownPickerView.isHidden = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.cursusTextField {
            self.dropDownPickerView.isHidden = false
            cursusTextField.endEditing(true)
        }
    }
    
    /**
    *   PROTOCOLS
    **/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.skillTableView {
            return currentSkills.count
        } else {
            return currentProjects.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.skillTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "skillCell") as? StudentTableViewCell
            cell?.displayStudent(skill: currentSkills[indexPath.row])
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "projectCell") as? ProjectTableViewCell
            cell?.displayProject(project: currentProjects[indexPath.row])
            return cell!
        }
    }
    
    func cursusIsChoosen(name: String) {
        for cursus in student.cursus_users {
            if cursus.cursus.name == name && cursus.cursus.id != currentCursusId {
                currentCursusId = cursus.cursus.id
                currentSkills = cursus.skills
                DispatchQueue.main.async {
                    self.skillTableView.reloadData()
                    self.skillTableView.endUpdates()
                }
                currentProjects.removeAll()
                for project in student.projects_users {
                    for cursusId in project.cursus_ids {
                        if cursusId == currentCursusId {
                            currentProjects.append(project)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.projectTableView.reloadData()
                    self.projectTableView.endUpdates()
                }
            }
        }
    }
    
    /**
     *   INITIALIZATION
     **/
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentCursusId = student.cursus_users[0].cursus_id
        loginLabel.text = student.login
        phoneNumberLabel.text = student.phone
        correctionPointLabel.text = "Points de corrections : " + String(student.correction_point)
        walletLabel.text = "Wallet : " + String(student.wallet)
        
        for project in student.projects_users {
            for cursusId in project.cursus_ids {
                if cursusId == currentCursusId {
                    currentProjects.append(project)
                }
            }
        }
        
        for cursus in student.cursus_users {
            listCursus.append(cursus.cursus.name)
            
            if cursus.cursus_id == currentCursusId {
                currentSkills = cursus.skills
                
                self.cursusTextField.text = cursus.cursus.name
                self.dropDownPickerView.isHidden = true
            }
        }
        
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator.center = self.studentImageView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.studentImageView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        DispatchQueue.global(qos: .background).async {
            if let imageUrl = URL(string: self.student.image_url) {
                if let imageData = try? Data(contentsOf: imageUrl) {
                    DispatchQueue.main.async {
                        self.studentImageView.image = UIImage(data: imageData)
                        activityIndicator.stopAnimating()
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
