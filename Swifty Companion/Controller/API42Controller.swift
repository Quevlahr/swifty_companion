//
//  API42Controller.swift
//  Swifty Companion
//
//  Created by Quentin ROULON on 10/15/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

class API42Controller {
    
    let delegate: API42Delegate?
    var token: String = ""
    var expires: Int = 0
    let host: String = "https://api.intra.42.fr"
    let customer_uid: String = "0b7e74df9229228001fc75bb13421ed0ebde0f3efc5b71a684f75073a5ca05e9"
    let customer_secret : String = "4dbdc22bac8e9f507abd3826a56c6dc9573735846bcf11aad6fe2a9cc3b92753"
    
    let token_route: String = "/oauth/token"
    let users_route: String = "/v2/users"
    
    init(delegate: API42Delegate) {
        self.delegate = delegate
    }
    
    func getToken(completion: @escaping () -> Void) {
        guard let url = URL(string: host + token_route + "?grant_type=client_credentials&client_id=" + customer_uid + "&client_secret=" + customer_secret) else {
            self.delegate?.apiError(error: nil, message: "An error occured")
            return
        }
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if let err = error {
                self.delegate?.apiError(error: err, message: "1 - An error occurred for getToken")
                self.getToken(completion: completion)
            }
            else if let d = data {
                do {
                    let decoder = JSONDecoder()
                    if let response = try decoder.decode(Token.self, from: d) as Token? {
                        self.token = response.access_token
                        self.expires = response.expires_in
                        completion()
                    }
                }
                catch (let err) {
                    self.delegate?.apiError(error: err, message: "2 - An error occurred for getToken")
                    self.getToken(completion: completion)
                }
            }
        }
        task.resume()
    }
    
    func getUser(login: String, completion: @escaping (_ student: Student) -> Void) {
        guard let url = URL(string: host + users_route + "/" + login) else {
            self.delegate?.apiError(error: nil, message: "An error occured")
            return
        }

        let request = NSMutableURLRequest(url: url)
        request.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if let err = error {
                self.delegate?.apiError(error: err, message: "1 - An error occurred for getUser")
            }
            else if let d = data {
                do {
                    let decoder = JSONDecoder()
                    if let student = try decoder.decode(Student.self, from: d) as Student? {
                        completion(student)
                    }
                }
                catch (let err) {
                    self.delegate?.apiError(error: err, message: "This user does not exists")
                }
            }
        }
        task.resume()
    }
}
