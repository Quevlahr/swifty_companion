//
//  ViewController.swift
//  Swifty Companion
//
//  Created by Quentin ROULON on 10/15/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import UIKit

protocol API42Delegate: NSObjectProtocol {
    func apiError(error: Error?, message: String)
}

class ViewController: UIViewController, UITextFieldDelegate {
    
    var backgroundImageView: UIImageView!
    let backgroundImage = UIImage(named: "background_coalition")
    let segueToStudent: String = "segueToStudent"
    var apiController: API42Controller?
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchButtonOutlet: UIButton!
    
    @IBAction func searchButton(_ sender: UIButton) {
        guard let searchText = searchTextField.text else { return }
        let login = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        if !login.isEmpty {
            self.toggleSearch()
            apiController?.getUser(login: login, completion: {
                (student: Student) -> Void in
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: self.segueToStudent, sender: student)
                }
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchButton(searchButtonOutlet)
        textField.resignFirstResponder()
        return true
    }
    
    func toggleSearch() {
        if self.searchTextField.isEnabled == true {
            self.searchActivityIndicator.startAnimating()
            self.searchTextField.isEnabled = false
            self.searchButtonOutlet.isEnabled = false
        } else {
            self.searchActivityIndicator.stopAnimating()
            self.searchTextField.isEnabled = true
            self.searchButtonOutlet.isEnabled = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundImageView = UIImageView(frame: self.view.bounds)
        backgroundImageView.contentMode =  UIViewContentMode.scaleAspectFill
        backgroundImageView.image = backgroundImage
        backgroundImageView.center = view.center
        view.addSubview(backgroundImageView)
        self.view.sendSubview(toBack: backgroundImageView)
        
        self.searchTextField.delegate = self
        toggleSearch()
        
        self.apiController = API42Controller(delegate: self)
        apiController?.getToken(completion: {
            DispatchQueue.main.async {
                self.toggleSearch()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueToStudent {
            if let student = sender as? Student {
                if let viewController = segue.destination as? StudentViewController {
                    viewController.student = student
                    self.toggleSearch()
                }
            }
        }
    }
}

extension ViewController: API42Delegate {
    func apiError(error: Error?, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            self.toggleSearch()
        }
    }
}

