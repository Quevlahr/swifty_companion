//
//  Token.swift
//  Swifty Companion
//
//  Created by Quentin ROULON on 10/15/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

struct Token: Codable {

    let access_token: String
    let token_type: String
    let expires_in: Int
    let scope: String
    let created_at: Int
}
