//
//  User.swift
//  Swifty Companion
//
//  Created by Quentin ROULON on 10/15/18.
//  Copyright © 2018 Quentin ROULON. All rights reserved.
//

import Foundation

struct Student: Codable {
    let id: Int
    let login: String
    let url: String
    let last_name: String
    let first_name: String
    let correction_point: Int
    let image_url: String
    let phone: String
    let email: String
    let wallet: Int
    let location: String?
    let cursus_users: [Cursus]
    let projects_users: [Project]
}

struct Cursus: Codable {
    let cursus: CursusDetails
    let level: Double
    let cursus_id: Int
    let skills: [Skill]
}

struct CursusDetails: Codable {
    let id: Int
    let name: String
}

struct Skill: Codable {
    let level: Double
    let name: String
}

struct Project: Codable {
    let final_mark: Int?
    let project: ProjectName
    let status: String
    let validated: Int?
    let cursus_ids: [Int]
}

struct ProjectName: Codable {
    let name: String
}

